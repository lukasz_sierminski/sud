% Wioleta Zakrzewska
% Aleksandra Gandziarowska
% Lukasz Sierminski
% Maciej Milewski
clear
tSin= 5;                            % okres sygnalu
time = 10;                          % czas
samplingRate = 0.1;                 % okres probkowania
quantizationLvls = 16;              % ilosc poziomow kwantowania
bits = 4;                           % ilosc bitow potrzebnych do zapisu probki
interferenceBit = 21;               % okresla co ktory bit symuluje zaklocenie

x = 0:samplingRate:time;
y = sin(x*(2*pi)/tSin);

figure
set(gcf, 'Position', [300, 100, 1200, 800])
hold on
plot(x, y, 'Color', 'red', 'DisplayName', 'Sygnal analogowy')
signalStem = stem(x, y, 'filled', 'Color', 'red', 'MarkerSize', 4);

xlim([0 time]);
ylim([-2 quantizationLvls]);

% kwantowanie
y = uencode(y, log2(quantizationLvls));

stairs(x, y, 'Color', [0.206 0.5 1], 'LineWidth', 1.4, 'DisplayName', 'Sygnal kwantowany')
stem(x, y, 'filled', 'MarkerSize', 4, 'Color', [0.217 0.61 1])
uistack(signalStem, 'top')

hold off

% zamiana na ciag bitow
decValuesInBin = dec2bin(y, bits);
decValuesInBin = transpose(decValuesInBin);
binarySequence = reshape(decValuesInBin,[1 length(y)*bits]);

% zaklocenia
for i = interferenceBit:interferenceBit:length(binarySequence)
   binarySequence(i) = '1';
end

% odkodowanie 
binaryArray = vec2mat(binarySequence, bits);
decodedSignal = bin2dec(binaryArray);

figure
set(gcf, 'Position', [300, 100, 1200, 800])
hold on
xlim([0 time]);
ylim([-2 quantizationLvls]);
stairs(x, y, 'Color', [0.206 0.5 1], 'LineWidth', 1.4)
stairs(x, decodedSignal, 'Color', 'red', 'LineWidth', 1.4)

legend('Sygnal w nadajniku','Sygnal w odbiorniku','Location','southwest')

hold off